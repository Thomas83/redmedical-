import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-w-item',
  templateUrl: './w-item.component.html',
  styleUrls: ['./w-item.component.scss']
})
export class WItemComponent implements OnInit {

  @Input() wItem;

  constructor() { }

  ngOnInit() {
  }

}
