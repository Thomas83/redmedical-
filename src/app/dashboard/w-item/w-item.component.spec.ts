import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WItemComponent } from './w-item.component';

describe('WItemComponent', () => {
  let component: WItemComponent;
  let fixture: ComponentFixture<WItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
