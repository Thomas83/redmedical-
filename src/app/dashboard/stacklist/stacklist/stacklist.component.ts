import { Component, OnInit } from '@angular/core';
import { ISearchResultItem } from 'app/core/services/search.service'
import { SearchService } from 'app/core/services/search.service';
import weatherdata from 'app/core/services/weatherdata.json';


@Component({
  selector: 'app-stacklist',
  templateUrl: './stacklist.component.html',
  styleUrls: ['./stacklist.component.scss']
})
export class StacklistComponent implements OnInit {
    stackOverFlowItems: ISearchResultItem [] = [];
    angular2Items: ISearchResultItem [] = [];
    tempWeatherList: ISearchResultItem [] = [];
    weatherItems = weatherdata;
    weatherList = [];
    expansionPanel = false;

  constructor(private searchService: SearchService) {
   }

  ngOnInit() {
  this.loadThemeTypeScript();
  this.loadAngular2();
  this.loadWeather();
}


loadThemeTypeScript(){
  this.searchService.search('Typescript')
      .subscribe((item) => {
         this.stackOverFlowItems = item['items'];
      }, (error) => {
          console.log(error);
      })
}
loadAngular2(){
  this.searchService.search('Angular2')
      .subscribe((item) => {
         this.angular2Items = item['items'];
      }, (error) => {
          console.log(error);
      })
}
loadWeather(){
  this.searchService.search('Weather')
      .subscribe(item => {
          this.tempWeatherList = item['items'];
          let max = this.weatherItems.length -1;
          let index: number;
          for (index = 0; index < 10; index++) {
              if (index % 2 == 0) {
                  this.weatherList.push(this.tempWeatherList[index / 2]);
              } else {
                  this.weatherList.push(weatherdata[Math.floor(Math.random() * max)]);
              }
          }
      }, error => {
          console.log(error);
      })
}
}




  
  


