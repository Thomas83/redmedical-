import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StacklistComponent } from './stacklist.component';

describe('StacklistComponent', () => {
  let component: StacklistComponent;
  let fixture: ComponentFixture<StacklistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StacklistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StacklistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
