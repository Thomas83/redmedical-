import { TestBed } from '@angular/core/testing';

import { OauthStackOverFlowService } from './oauth-stack-over-flow.service';

describe('OauthStackOverFlowService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OauthStackOverFlowService = TestBed.get(OauthStackOverFlowService);
    expect(service).toBeTruthy();
  });
});
