import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import { Observable } from "rxjs";
import {map} from 'rxjs/operators';
import { OauthStackOverFlowService } from '../services/oauth-stack-over-flow.service';


export interface ISearchResultItem  {
    answer_count: number;
    creation_date: Date;
    link: string;
    owner: {
    display_name: string;
    link: string;
    }
    tags: string[];
    view_count: number;
    title: string;
}



@Injectable()
export class SearchService {
    oAuthkey: string;
    private static readonly apiUrl =
    "https://api.stackexchange.com/2.2/search?pagesize=10&order=desc&sort=activity&site=stackoverflow&key="

    constructor(private http: Http, private OauthStackOverFlowService: OauthStackOverFlowService) {
    }


    search(keyword: string): Observable<JSON> {
        this.oAuthkey = this.OauthStackOverFlowService.getOAuthKey();
        return this.http.get(
            SearchService.apiUrl + this.oAuthkey + '&intitle=' + keyword)
            .pipe(map((res: Response) => {
                let data = res.json();
                console.log("API USAGE: " + data.quota_remaining + " of " + data.quota_max + " requests available" );
                return data;
            }));
        }

}
