import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import {SearchService} from "./core/services/search.service";
import {AppRoutingModule} from "./app.routing.module";
import { DashboardComponent } from './dashboard/dashboard.component';
import {LayoutModule} from "./core/layout/layout.module";
import { SearchComponent } from './search/search.component';
import { StacklistComponent } from './dashboard/stacklist/stacklist/stacklist.component';
import {MatCardModule, MatGridListModule, MatChipsModule, MatBadgeModule,  MatListModule, MatExpansionModule, MatButtonModule} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ItemComponent } from './dashboard/item/item.component';
import { WItemComponent } from './dashboard/w-item/w-item.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SearchComponent,
    StacklistComponent,
    ItemComponent,
    WItemComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    LayoutModule,
    AppRoutingModule,
    MatListModule,
    MatCardModule,
    MatGridListModule,
    MatBadgeModule,
    MatChipsModule,
    MatExpansionModule,
    MatButtonModule,
  ],
  providers: [SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
